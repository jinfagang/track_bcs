//
// Created by jintian on 17-8-5.
//

#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>
#include <map>
#include <cstdio>
#include <vector>
#include "opencv2/imgcodecs.hpp"
#include <cv.hpp>

#include "../include/track_bcs.h"
#include "../lib/colors.h"
#include "../lib/t_string.h"
#include "../lib/t_os.h"
#include "../lib/t_log.h"
#include "../lib/t_string.h"

// include rapid json library
#include "../include/rapidjson/document.h"
#include "../include/rapidjson/writer.h"
#include "../include/rapidjson/stringbuffer.h"
#include "../include/rapidjson/filereadstream.h"

#include "../include/track_colors.h"



using namespace std;
using namespace rapidjson;

void drawTrackBoxOnImage(cv::Mat &image, struct TrackBBox &t_bb, TrackColors trackColors);
map<string, vector<TrackBBox> > loadTrackBoxFromJson(string path);
void parse_seq_info(char* seq_info, vector<string> &cameras_v);


void show_multi_views_track(char *seq_info){
    // this method for show multi views camera
    vector<string> cameras;
    int start_frame = 16015, end_frame = 16024;
    parse_seq_info(seq_info, cameras);
    LogInfo(cameras[0]);

    int views = (int) cameras.size();
    cout << "got camera views: " << cameras.size() << endl;

    string root_dir = t_os::parent_path(seq_info);
    cout << "root dir " << root_dir << endl;
    string annotations_dir = t_os::join(&root_dir[0u], "annotations");
    cout << "annotations dir " << annotations_dir <<endl;

    vector<string> json_files;
    for (int i = 0; i < cameras.size(); ++i) {
        cout << t_os::join(&annotations_dir[0u], cameras[i] + ".json") << endl;
        json_files.push_back(t_os::join(&annotations_dir[0u], cameras[i] + ".json"));
    }

    vector<map<string, vector<TrackBBox> >> all_camera_data;
    for (int j = 0; j < json_files.size(); ++j) {
        map<string, vector<TrackBBox> > one_camera_data = loadTrackBoxFromJson(json_files[j]);
        all_camera_data.push_back(one_camera_data);
    }

    TrackColors trackColors;

    string camera_6mm_window = "camera 6mm";
    string camera_12_5mm_window = "camera 12.5mm";
    string camera_25mm_window = "camera 25mm";
    string camera_50mm_window = "camera 50mm";
    int window_width = 800;
    int window_height = 500;
    int off_set_left = 40;
    int off_set_top = 10;

    for (auto it = all_camera_data[0].begin(); it != all_camera_data[0].end() ; it++) {
        string image_file_6mm = it->first;
        string image_file_12_5mm = it->first;
        string image_file_25mm = it->first;
        string image_file_50mm = it->first;

        Replace(image_file_12_5mm, "6", "12_5");
        Replace(image_file_25mm, "6", "25");
        Replace(image_file_50mm, "6", "50");

        vector<TrackBBox> t_bb_6mm = it->second;
        // read 6mm and draw box on it
        if (t_os::exists(&image_file_6mm[0u])) {
            cv::Mat image = cv::imread(image_file_6mm, cv::IMREAD_COLOR);
            if (image.data) {
                // iterate vector to draw a box on image
                for (auto it_b = t_bb_6mm.begin(); it_b != t_bb_6mm.end(); ++it_b) {
                    TrackBBox current_box = *it_b;
                    drawTrackBoxOnImage(image, current_box, trackColors);
                }
                LogInfo(image.rows);
                LogInfo(image.cols);
                cv::namedWindow(camera_6mm_window, cv::WINDOW_NORMAL);
                cv::resizeWindow(camera_6mm_window, window_width, window_height);
                cv::moveWindow(camera_6mm_window, off_set_left, off_set_top);
                cv::imshow(camera_6mm_window, image);
            } else {
                LogFatal("image " + image_file_6mm + " not exist.");
            }
            LogInfo(camera_6mm_window + " draw finished.");
        }

        if (all_camera_data[1].find(image_file_12_5mm) != all_camera_data[1].end()) {
            vector<TrackBBox> t_bb_12_5mm = all_camera_data[1].find(image_file_12_5mm)->second;
            // read 12.5 mm and draw box on it
            if (t_os::exists(&image_file_12_5mm[0u])) {
                cv::Mat image = cv::imread(image_file_12_5mm, cv::IMREAD_COLOR);
                if (image.data) {
                    // iterate vector to draw a box on image
                    for (auto it_b = t_bb_12_5mm.begin(); it_b != t_bb_12_5mm.end(); ++it_b) {
                        TrackBBox current_box = *it_b;
                        drawTrackBoxOnImage(image, current_box, trackColors);
                    }
                    cv::namedWindow(camera_12_5mm_window, cv::WINDOW_NORMAL);
                    cv::moveWindow(camera_12_5mm_window, off_set_left + window_width, off_set_top);
                    cv::resizeWindow(camera_12_5mm_window, window_width, window_height);
                    cv::imshow(camera_12_5mm_window, image);
                } else {
                    LogFatal("image " + image_file_12_5mm + " not exist.");
                }
                LogInfo(camera_12_5mm_window + " draw finished.");
            }


        } else {
            LogFatal("Can not find track bbox from 12.5mm, where image file is " + image_file_12_5mm);
        }
        if (all_camera_data[2].find(image_file_25mm) != all_camera_data[2].end()) {
            vector<TrackBBox> t_bb_25mm = all_camera_data[2].find(image_file_25mm)->second;
            // read 25 mm and draw box on it
            if (t_os::exists(&image_file_25mm[0u])) {
                cv::Mat image = cv::imread(image_file_25mm, cv::IMREAD_COLOR);
                if (image.data) {
                    // iterate vector to draw a box on image
                    for (auto it_b = t_bb_25mm.begin(); it_b != t_bb_25mm.end(); ++it_b) {
                        TrackBBox current_box = *it_b;
                        drawTrackBoxOnImage(image, current_box, trackColors);
                    }
                    cv::namedWindow(camera_25mm_window, cv::WINDOW_NORMAL);
                    cv::resizeWindow(camera_25mm_window, window_width, window_height);
                    cv::moveWindow(camera_25mm_window, off_set_left, 3*off_set_top + window_height);
                    cv::imshow(camera_25mm_window, image);
                } else {
                    LogFatal("image " + image_file_25mm + " not exist.");
                }
                LogInfo(camera_25mm_window + " draw finished.");
            }

        } else {
            LogFatal("Can not find track bbox from 25mm, where image file is " + image_file_25mm);
        }
        if (all_camera_data[3].find(image_file_50mm) != all_camera_data[3].end()) {
            vector<TrackBBox> t_bb_50mm = all_camera_data[3].find(image_file_50mm)->second;
            // read 50 mm and draw box on it
            if (t_os::exists(&image_file_50mm[0u])) {
                cv::Mat image = cv::imread(image_file_50mm, cv::IMREAD_COLOR);
                if (image.data) {
                    // iterate vector to draw a box on image
                    for (auto it_b = t_bb_50mm.begin(); it_b != t_bb_50mm.end(); ++it_b) {
                        TrackBBox current_box = *it_b;
                        drawTrackBoxOnImage(image, current_box, trackColors);
                    }
                    cv::namedWindow(camera_50mm_window, cv::WINDOW_NORMAL);
                    cv::resizeWindow(camera_50mm_window, window_width, window_height);
                    cv::moveWindow(camera_50mm_window, off_set_left + window_width, 3*off_set_top + window_height);
                    cv::imshow(camera_50mm_window, image);
                } else {
                    LogFatal("image " + image_file_50mm + " not exist.");
                }
                LogInfo(camera_50mm_window + " draw finished.");
            }

        } else {
            LogFatal("Can not find track bbox from 50mm, where image file is " + image_file_50mm);
        }

        // waite key to hold image
        while (true) {
            char c = (char) cv::waitKey();
            if (c == 'q'){
                cv::destroyAllWindows();
                exit(0);
            } else if (c == 'n') {
                cv::destroyAllWindows();
                cout << "pressed n\n";
                break;
            } else {
                continue;
            }
        }

    }


}

void parse_seq_info(char* seq_info, vector<string> &cameras_v) {
    // this will parse seq info, return json file path
    FILE *fp = fopen(seq_info, "r");
    char read_buffer[6556];
    FileReadStream frs(fp, read_buffer, sizeof(read_buffer));
    Document d;
    d.ParseStream(frs);
    fclose(fp);

    string cameras = d["img_dir"].GetString();
    SplitString(cameras, cameras_v, ",");
}

map<string, vector<TrackBBox> > loadTrackBoxFromJson(string path) {
    // this method will load an map which key is a single image file
    // and value is vector of track boxes
    // this read JSON file will using RapidJson library
    if (!t_os::exists(&path[0u])) {
        LogFatal("file does not exist. " + path);
        exit(0);
    }

    string annotations_dir = t_os::parent_path(&path[0u]);
    string root_dir = t_os::parent_path(&annotations_dir[0u]);

    FILE *fp = fopen(&path[0u], "r");
    char read_buffer[6556];
    FileReadStream frs(fp, read_buffer, sizeof(read_buffer));
    Document d;
    d.ParseStream(frs);
    fclose(fp);

    map<string, vector<TrackBBox> > image_t_bb_map;
    int frames_count = 0;
    // Parse JSON
    if (d.IsArray()){
        const Value& d_array = d.GetArray();
        assert(d_array.IsArray());
        frames_count = d_array.Size();

        for (SizeType i = 0; i < d_array.Size(); i++) {
            const Value& one_frame = d_array[i];
            if (one_frame.IsObject()) {
                const Value& annotations = one_frame["annotations"];
                const Value& class_name = one_frame["class"];
                const Value& file_path = one_frame["filename"];

                string class_name_s = class_name.GetString();
                string file_path_s = file_path.GetString();
                // convert ..\camera_6mm\frame16020.jpg to root_dir/camera_6mm/frame1602.jpg
                ReplaceAll(file_path_s, "\\", "/");
                Replace(file_path_s, "../", "");
                file_path_s = t_os::join(&root_dir[0u], file_path_s);

                // iterator annotation array, put this into image_t_bb_map
                for (SizeType  j = 0; j < annotations.GetArray().Size(); ++j) {
                    const Value& one_annotation = annotations.GetArray()[j];

                    TrackBBox t_bb;
                    t_bb.track_id = one_annotation["ID"].GetInt();
                    t_bb.class_name = one_annotation["class"].GetString();
                    t_bb.bb_left = one_annotation["x"].GetDouble();
                    t_bb.bb_top = one_annotation["y"].GetDouble();
                    t_bb.bb_width = one_annotation["width"].GetDouble();
                    t_bb.bb_height = one_annotation["height"].GetDouble();


                    map<string, vector<TrackBBox> >::iterator it = image_t_bb_map.find(file_path_s);
                    if (it == image_t_bb_map.end()) {
                        // image_name not in map, new a key-value pair
                        vector<TrackBBox> t_bb_vector;
                        t_bb_vector.push_back(t_bb);
                        image_t_bb_map.insert(pair<string, vector<TrackBBox>>(file_path_s, t_bb_vector));
                    } else {
                        // if image_name already in map, find this key value push_back into vector
                        vector<TrackBBox> current_image_t_bb = image_t_bb_map[file_path_s];
                        current_image_t_bb.push_back(t_bb);
                        image_t_bb_map[file_path_s] = current_image_t_bb;
                    }
                }

            } else {
                LogFatal("json parse error!");
            }
        }
    } else {
        LogFatal("json parse error!");
    }

    return image_t_bb_map;
}


void drawTrackBoxOnImage(cv::Mat &image, struct TrackBBox &t_bb, TrackColors trackColors) {
    // this method draw track box on image
    if (!image.data) {
        cout << colors::red << "[BAD] image seems not valid. " << colors::reset << endl;
    } else {

        // draw an box on this image
        int font_face = cv::FONT_HERSHEY_SIMPLEX;
        double font_scale = 1.2;
        int thickness = 3;

        cv::Scalar track_color = trackColors.GetUniqueColorByTrackId(t_bb.track_id);

        cv::Point text_origin = cv::Point((int)t_bb.bb_left, (int)t_bb.bb_top);
        cv::Rect bbox_rect = cv::Rect((int)t_bb.bb_left, (int)t_bb.bb_top, (int)t_bb.bb_width, (int)t_bb.bb_height);

        cv::putText(image, "ID" + to_string(t_bb.track_id) + " " + t_bb.class_name, text_origin,
                    font_face, font_scale, track_color, thickness, 8);
        cv::rectangle(image, bbox_rect, track_color, 2, 8, 0);
    }
}


