//
// Created by jintian on 17-8-6.
//

#include "../include/track_colors.h"


using namespace std;

TrackColors::TrackColors() {
    map<int, cv::Scalar> colorMap;
    colorMap[0] = cv::Scalar(25, 12, 0);
    colorMap[1] = cv::Scalar(255, 255, 0);
    colorMap[2] = cv::Scalar(0, 0, 255);
    colorMap[3] = cv::Scalar(255, 255, 0);
    colorMap[4] = cv::Scalar(0, 255, 255);
    colorMap[5] = cv::Scalar(255, 0, 255);
    colorMap[6] = cv::Scalar(128, 128, 0);
    colorMap[7] = cv::Scalar(128, 0, 0);
    colorMap[8] = cv::Scalar(128, 0, 128);
    colorMap[9] = cv::Scalar(255, 20, 147);
    colorMap[10] = cv::Scalar(210, 105, 30);
    colorMap[11] = cv::Scalar(0, 0, 255);
    colorMap[12] = cv::Scalar(72, 209, 204);
    colorMap[13] = cv::Scalar(255, 255, 0);
    colorMap[14] = cv::Scalar(255, 69, 0);
    colorMap[15] = cv::Scalar(0, 0, 255);
    colorMap[16] = cv::Scalar(0, 255, 255);
    colorMap[17] = cv::Scalar(255, 0, 255);
    colorMap[18] = cv::Scalar(210, 105, 30);
    colorMap[19] = cv::Scalar(128, 0, 0);
    colorMap[20] = cv::Scalar(255, 0, 0);
    colorMap[21] = cv::Scalar(0, 255, 0);
    colorMap[22] = cv::Scalar(0, 0, 255);
    colorMap[23] = cv::Scalar(255, 255, 0);
    colorMap[24] = cv::Scalar(0, 255, 255);
    colorMap[25] = cv::Scalar(255, 0, 255);
    colorMap[26] = cv::Scalar(128, 128, 0);
    colorMap[27] = cv::Scalar(128, 0, 0);
    colorMap[28] = cv::Scalar(128, 0, 128);
    colorMap[29] = cv::Scalar(255, 20, 147);
    colorMap[30] = cv::Scalar(210, 105, 30);
    this->colorMap = colorMap;
}

TrackColors::~TrackColors() {
    cout << "Should erase this class in here" << endl;
}

cv::Scalar TrackColors::GetUniqueColorByTrackId(int track_id) {
    // this method will get a unique color by track id
    auto it = this->colorMap.find(track_id);
    if (it != this->colorMap.end()) {
        // find color
        return it->second;
    } else {
        // not find, return a default color
        return cv::Scalar(255, 255, 0);
    }
}