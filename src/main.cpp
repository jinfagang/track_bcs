#include <iostream>
#include <string>
#include "../include/motto.h"
#include "../lib/colors.h"
#include "opencv2/opencv.hpp"

#include "../lib/t_os.h"
#include "../lib/t_string.h"

#include "../include/track_bcs.h"

using namespace std;
using namespace t_os;


int main(int argc, char *argv[]) {
    cout << colors::red << "Track-BCS - Tracking Bad Case Selecting By Auto-Lab.\n" << colors::reset;
    string motto = getMotto();
    cout << colors::blue << "[motto] " << motto << colors::reset << endl;

    if (argc < 2) {
        throw invalid_argument("Please specific seq_info.json file path.");
    } else {
        cout << colors::green << "load from: " << argv[1] << colors::reset << endl;

        show_multi_views_track(argv[1]);
    }

    return 0;
}