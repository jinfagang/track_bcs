//
// Created by jintian on 17-8-5.
//

#include "t_log.h"
#include "colors.h"


using namespace std;


void LogInfo(int msg){
    cout << colors::blue << "[INFO] " <<  colors::reset << __TIME__ << ": " << __LINE__ << ": " <<  msg << colors::reset << endl;
}


void LogInfo(const std::string& msg){
    cout << colors::blue << "[INFO] " <<  colors::reset << __TIME__ << ": " << __LINE__ << ": "<< msg << colors::reset << endl;
}

/**
 * this method is used for logging errors
 * @param msg
 */
void LogFatal(std::string msg) {
    cout << colors::red << "[ERROR] " << colors::yellow << msg << colors::reset << endl;
}

void LogFatal(int msg) {
    cout << colors::red << "[ERROR] " << colors::yellow << msg << colors::reset << endl;
}



