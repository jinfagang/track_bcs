//
// Created by jintian on 17-8-5.
//

#ifndef DEDACKER_TRACK_BCS_H
#define DEDACKER_TRACK_BCS_H

#include <iostream>
#include <string>
#include "track_colors.h"

using namespace std;

struct TrackBBox {
    int track_id;
    int class_index;
    string class_name;
    double bb_left;
    double bb_top;
    double bb_width;
    double bb_height;

    double confidence;
    double visibility_ratio;
};

void show_multi_views_track(char *seq_info);
void show_4_views(char* seq_info);



#endif //DEDACKER_TRACK_BCS_H
