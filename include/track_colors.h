//
// Created by jintian on 17-8-6.
//

#ifndef TRACK_BCS_TRACK_COLORS_H
#define TRACK_BCS_TRACK_COLORS_H

#include <iostream>
#include <map>
#include <vector>
#include "opencv2/core.hpp"

using namespace std;

// track colors class, provide unique color by track id
class TrackColors{
public:

    TrackColors();
    ~TrackColors();

    cv::Scalar GetUniqueColorByTrackId(int track_id);

private:
    map<int,cv::Scalar> colorMap;

};

#endif //TRACK_BCS_TRACK_COLORS_H
